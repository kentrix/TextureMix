package kentrix.texturemix;

import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class Config {

    protected static final String CATEGORY_VARIOUS_SETTINGS = "Settings";
    private static final int ALLOWED_RESOLUTION[] = {8, 16, 32, 64, 128, 256, 512, 1024, 2048};
    protected Configuration configuration;

    protected Config() {
    }

    protected void initialize(final File configFile) {
        configuration = new Configuration(configFile);
    }

    protected void load() {
        configuration.load();
    }

    protected void save() {
        configuration.save();
    }

    public int getMiscInteger(final String key, final int defaultValue) {
        return getMiscInteger(key, defaultValue, null);
    }

    public int getMiscInteger(final String key, final int defaultValue, final String comment) {
        final Property property = configuration.get(CATEGORY_VARIOUS_SETTINGS, key, defaultValue, comment);
        return property.getInt(defaultValue);
    }

    public double getMiscDouble(final String key, final double defaultValue) {
        return getMiscDouble(key, defaultValue, null);
    }

    public double getMiscDouble(final String key, final double defaultValue, final String comment) {
        final Property property = configuration.get(CATEGORY_VARIOUS_SETTINGS, key, defaultValue, comment);
        return property.getDouble(defaultValue);
    }

    public boolean getMiscBoolean(final String key, final boolean defaultValue) {
        return getMiscBoolean(key, defaultValue, null);
    }

    public boolean getMiscBoolean(final String key, final boolean defaultValue, final String comment) {
        final Property property = configuration.get(CATEGORY_VARIOUS_SETTINGS, key, defaultValue, comment);
        return property.getBoolean(defaultValue);
    }

    public List<String> getMiscStrings(final String key, final List<String> defaultValue) {
        return getMiscStrings(key, defaultValue, null);
    }

    public List<String> getMiscStrings(final String key, final List<String> defaultValue, final String comment) {
        final Property property = configuration.get(CATEGORY_VARIOUS_SETTINGS, key, defaultValue.toArray(new String[defaultValue.size()]), comment);
        return Arrays.asList(property.getStringList());
    }


    public Configuration getConfiguration() {
        return configuration;
    }



}
