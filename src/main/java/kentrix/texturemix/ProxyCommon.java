package kentrix.texturemix;

import net.minecraftforge.common.MinecraftForge;

public class ProxyCommon {
    private int resolution;

    public ProxyCommon() {
    }

    protected void loadConfigValues() {
        resolution = TextureMixMain.config.getMiscInteger("resolution", 128);
    }


    public void onPreInit() {
        MinecraftForge.EVENT_BUS.register(this);
    }

    public void onInit() {
    }

    public void onPostInit() {
    }
}
