package kentrix.texturemix;

public interface Initable {
  void initialise();
}
