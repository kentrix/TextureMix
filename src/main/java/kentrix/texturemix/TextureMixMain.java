package kentrix.texturemix;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.event.FMLServerStoppedEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;

import kentrix.texturemix.misc.Crafting;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.StringFormatterMessageFactory;

@Mod(modid = TextureMixMain.MOD_ID, name =TextureMixMain.MOD_NAME, version = TextureMixMain.MOD_VERSION)
public class TextureMixMain {

    public static final Logger logger = LogManager.getLogger(TextureMixMain.class.getSimpleName(), new StringFormatterMessageFactory());
    private static final Config config = new Config();

    public static final String MOD_NAME = "TextureMix";
    public static final String MOD_ID = "texturemix";
    public static final String MOD_VERSION = "pre-rel";

    @Mod.Instance(MOD_ID)
    public static  TextureMixMain instance; // this will point to the actual instance of this class once running

    @SidedProxy(clientSide = "kentrix.texturemix.ProxyClient", serverSide = "kentrix.texturemix.ProxyServer")
    public static ProxyCommon proxy;

    public static final SimpleNetworkWrapper network = NetworkRegistry.INSTANCE.newSimpleChannel("texturemix");
    public static final SimpleNetworkWrapper networkData = NetworkRegistry.INSTANCE.newSimpleChannel("texturemix_data");

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        config.initialize(event.getSuggestedConfigurationFile());
        proxy.onPreInit();
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        config.load();
        proxy.onInit();

        Crafting.instance.registerRecipes();

        config.save();
    }

    @Mod.EventHandler
    public void postInit(final FMLPostInitializationEvent event) {
        proxy.onPostInit();
    }

    @Mod.EventHandler
    public void serverStopped(final FMLServerStoppedEvent event) {
    }
}
